package com.adnan.weatherapp.model;


import java.util.ArrayList;

/**
 * Global singleton model object.
 *
 * @author Adnan Fakhar
 * @date 05-28-2019
 */
public final class Model {
    private static Model instance = null;
    private ArrayList<ForecastData> forecastData;
    private Forecast forecast;


    /**
     * Get four days forecast data
     * @return
     */
    public ArrayList<ForecastData> getForecastData() {
        return forecastData;
    }

    /**
     * set four days forecast data
     * @param forecastData
     */
    public void setForecastData(ArrayList<ForecastData> forecastData) {
        this.forecastData = forecastData;
    }



    public static void deleteInstance() {
        instance = null;
    }

    /**
     * Get singleton instance.
     */
    public static Model getInstance() {
        if (instance == null)
            instance = new Model();
        return instance;
    }

    /**
     * get current Temp
     * @return
     */
    public Forecast getForecast() {
        return forecast;
    }

    /**
     * set current Temp
     * @param forecast
     */
    public void setForecast(Forecast forecast) {
        this.forecast = forecast;
    }
}



