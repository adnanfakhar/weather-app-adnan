package com.adnan.weatherapp.model;

/**
 * Forecast.
 * Model class that holds current temp data
 *
 * @author Adnan Fakhar
 * @date 06-09-2019
 */

public class Forecast {

    private String currentTemp;

    /**
     * Get Temperature in centigrade
     *
     * @return
     */
    public String getCurrentTemp() {
        return currentTemp;
    }

    /**
     * set Temperature in centigrade
     *
     * @param currentTemp
     */
    public void setCurrentTemp(String currentTemp) {
        this.currentTemp = currentTemp;
    }


}
