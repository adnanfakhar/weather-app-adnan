package com.adnan.weatherapp.model;


/**
 * ForecastData.
 * Model class that holds data for current day
 *
 * @author Adnan Fakhar
 * @date 06-09-2019
 */

public class ForecastData {
    private String date;
    private String day;
    private String temperature;


    /**
     * get day name
     * @return
     */
    public String getDay() {
        return day;
    }

    /**
     * set day name
     * @param day
     */
    public void setDay(String day) {
        this.day = day;
    }

    /**
     * get current day temperature in centigrade
     * @return
     */
    public String getTemperature() {
        return temperature;
    }

    /**
     * set current day temperature in centigrade
     * @param temperature
     */
    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    /**
     * get current date
     * @return
     */
    public String getDate() {
        return date;
    }

    /**
     * set current date
     * @param date
     */
    public void setDate(String date) {
        this.date = date;
    }

}
