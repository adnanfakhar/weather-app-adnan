package com.adnan.weatherapp.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.adnan.weatherapp.R;
import com.adnan.weatherapp.model.ForecastData;
import com.adnan.weatherapp.model.Model;
import com.adnan.weatherapp.net.Request;
import com.adnan.weatherapp.util.Helper;

import java.util.ArrayList;

/**
 * MainActivity.
 * Launcher Class
 *
 * @author Adnan Fakhar
 * @date 06-09-2019
 */

public class MainActivity extends AppCompatActivity {

    private FrameLayout main_layout;
    private ScrollView scrollview;
    private LinearLayout forecast_layout;
    private TextView centigrade_txt;
    private View errorView = null;
    private Button retry;

    /**
     * Called when the activity is first created.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        callRequest();


    }

    /**
     * Initialize the Platform object with a valid Context object.
     * This method must be called before calling any other method in this class.
     *
     */
    private void init() {
        main_layout = findViewById(R.id.main_layout);
        scrollview = findViewById(R.id.scrollview);
        centigrade_txt = findViewById(R.id.centigrade_txt);
        forecast_layout = findViewById(R.id.forecast_layout);
    }

    /**
     * call the forecast request
     */
    private void callRequest() {
        if (Helper.isInternetAvailable(this)) {
            Helper.showProgressDialog(this);
            Request forecast = new Request(this);
            forecast.forecastRequest();
        } else {
            onError();
        }
    }

    /**
     * show error message
     */
    public void onError() {
        Helper.closeDailog();
        if (errorView == null) {
            errorView = LayoutInflater.from(this).inflate(R.layout.error_layout, null);
            retry = errorView.findViewById(R.id.retry);

            retry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    main_layout.removeView(errorView);
                    callRequest();

                }
            });
        }

        main_layout.addView(errorView);

    }

    /**
     * Fill the data in the fields
     */
    public void populateData() {
        centigrade_txt.setText(Model.getInstance().getForecast().getCurrentTemp());
        ArrayList<ForecastData> list = Model.getInstance().getForecastData();
        for (int k = 0; k < list.size(); k++) {

            LayoutInflater inflater = getLayoutInflater();
            View view = inflater.inflate(R.layout.items, null);
            TextView day_txt = (view).findViewById(R.id.day_txt);
            TextView temp_txt = (view).findViewById(R.id.temp_txt);

            day_txt.setText(list.get(k).getDay());
            temp_txt.setText(list.get(k).getTemperature());

            forecast_layout.addView(view);
        }

        //animation that slide up from bottom
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.slide_up);
        scrollview.startAnimation(anim);

    }
}
