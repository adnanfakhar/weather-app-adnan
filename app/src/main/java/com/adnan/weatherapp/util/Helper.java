package com.adnan.weatherapp.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import com.adnan.weatherapp.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Helper.
 * Utility Class
 *
 * @author Adnan Fakhar
 * @date 06-09-2019
 */
public class Helper {

    private static AlertDialog dialog;
    private static AlertDialog.Builder builder;

    private static boolean linkStatus = false;

    // ViewpagerCellHeight
    private static int cellHeight;

    public static AlertDialog showMessage(Context cxt, String title, String msg) {
        if (!((Activity) cxt).isFinishing()) {

            // Creating and Building the Dialog
            builder = new AlertDialog.Builder(cxt);

            builder.setMessage(msg);
            builder.setTitle(title)

                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            closeDailog();
                        }
                    });

            dialog = builder.create();
            dialog.setCancelable(false);
            if (!((Activity) cxt).isFinishing())
                dialog.show();
        }

        return dialog;
    }


    /**
     * Show Gauge while fetching data from server
     */

    public static void showProgressDialog(Context cxt) {
        if (!((Activity) cxt).isFinishing()) {

            // Creating and Building the Dialog
            builder = new AlertDialog.Builder(cxt);
            builder.setCancelable(true);

            View status = LayoutInflater.from(cxt).inflate(R.layout.layout_progress, null);
            dialog = builder.create();
            dialog.setCancelable(false);
            dialog.setView(status, 0, 0, 0, 0);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.parseColor("#00000000")));
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            if (!((Activity) cxt).isFinishing())
                dialog.show();
        }
    }


    /**
     * Close Dialog
     */
    public static void closeDailog() {
        if (dialog != null) {
            if (dialog.isShowing()) { // 2.9 error remove
                dialog.cancel();
            }

            dialog.dismiss();
        }

    }

    /**
     * Return week day name from the given date
     */
    public static String getDayfromDate(String d) {
        String name = "";
        SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = inFormat.parse(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat outFormat = new SimpleDateFormat("EEEE");
        name = outFormat.format(date);

        return name;

    }

    /**
     * This method actually checks if device is connected to
     * internet(There is a possibility it's connected to a network but not to internet).
     */
    public static boolean isInternetAvailable(Context cxt) {
        ConnectivityManager cm = (ConnectivityManager) cxt.getSystemService(Context.CONNECTIVITY_SERVICE);
        // test for connection
        if (cm.getActiveNetworkInfo() != null
                && cm.getActiveNetworkInfo().isAvailable()
                && cm.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {
            return false;
        }
    }

}
