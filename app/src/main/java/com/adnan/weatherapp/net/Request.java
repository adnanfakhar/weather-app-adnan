package com.adnan.weatherapp.net;

import android.content.Context;
import android.util.Log;

import com.adnan.weatherapp.activities.MainActivity;
import com.adnan.weatherapp.model.Forecast;
import com.adnan.weatherapp.model.ForecastData;
import com.adnan.weatherapp.model.Model;
import com.adnan.weatherapp.util.Helper;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Request.
 * This class get data in json format from web service
 * and the parse data and hold in model class
 *
 * @author Adnan Fakhar
 * @date 06-09-2019
 */

public class Request {
    private RequestQueue requestQueue;
    private Context cxt;

    // URL link
    private final String URL = "http://api.apixu.com/v1/forecast.json?key=ea0b2ebea51b46ba85f101631192705&q=Karachi&days=5";

    /**
     * Constructor
     * @param cxt
     */
    public Request(Context cxt) {
        this.cxt = cxt;

        // initialize request
        requestQueue = Volley.newRequestQueue(cxt);


    }

    /**
     * Web Service call using Volley Library
     *
     */
    public void forecastRequest() {
        // prepare the Request
        JsonObjectRequest getRequest = new JsonObjectRequest(com.android.volley.Request.Method.GET, URL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Helper.closeDailog();

                        Forecast forecast = new Forecast();
                        ArrayList<ForecastData> listForecastData = new ArrayList();


                        try {
                            JSONObject jObj = new JSONObject(response.toString());
                            JSONObject currentObj = jObj.getJSONObject("current");
                            String currentTemp = currentObj.getString("temp_c");

                            forecast.setCurrentTemp(currentTemp + "\u00B0");

                            // set current forecast temp
                            Model.getInstance().setForecast(forecast);

                            JSONObject forecastObj = jObj.getJSONObject("forecast");
                            JSONArray forecastdayArray = (JSONArray) forecastObj.get("forecastday");

                            for (int k = 0; k < forecastdayArray.length(); k++) {
                                ForecastData forecastData = new ForecastData();

                                JSONObject obj = forecastdayArray.getJSONObject(k);

                                String date = obj.optString("date").trim();
                                forecastData.setDate(date);

                                // get day name from date
                                forecastData.setDay(Helper.getDayfromDate(date));

                                JSONObject dayObj = obj.getJSONObject("day");
                                forecastData.setTemperature(dayObj.optString("maxtemp_c").trim()+"C");

                                listForecastData.add(forecastData);
                            }

                            // ForecastData add in model
                            Model.getInstance().setForecastData(listForecastData);

                            // show 4 day forecast data
                            ((MainActivity) cxt).populateData();


                        } catch (Exception e) {
                            // JSON error
                            e.printStackTrace();
                            Helper.showMessage(cxt, "Parsing Error", "Cannot parse response." + e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ((MainActivity) cxt).onError();
                        Log.e("Error.Response", error.getMessage());
                    }
                }
        );

// add it to the RequestQueue
        requestQueue.add(getRequest);
    }


}
